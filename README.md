# Kubernetes Core Concepts

> The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", 
> "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", 
> "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document 
> are to be interpreted as described in BCP 14 [RFC2119](https://www.rfc-editor.org/rfc/rfc2119) [RFC8174](https://www.rfc-editor.org/rfc/rfc8174.txt) when, 
> and only when, they appear in all capitals, as shown here.

This the working project for the PluralSight course _Kubernetes for Developers: Core Concepts_ by Dan Wahlin. 

Section notes will be below. Commits will be tagged by Section number and lesson.

These notes will generally work in a Git Bash environment, but YMMV. Optimized for macOS.

## Section 2 — Kubernetes from a Developer Perspective

### General setup

#### Docker or minikube

Choose Docker when possible. Enable Docker Kubernetes support from settings and restart.

#### kubectl alias

Create a command shortcut for `kubectl` (Kubernetes Control).

```shell
alias k=kubectl
```

Oh-My-ZSH has a `kubernetes` plugin. Enable in `.zshrc` plugins list.

Powershell options also exist.

#### Install Web UI

```shell
k apply -f  https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml
```

#### Web UI token

```shell
k apply -f section2/dashboard.adminuser.yml
```

Generate a token for the UI as shown below.

```shell
k -n kubernetes-dashboard create token admin-user | pbcopy
```

`pbcopy` and `pbpaste` are macOS tools to capture output to the "clipboard." YMMV on Windows.

#### Proxy Web UI

```shell
k proxy & # note this puts the proxy service in the background.
```

Visit the [Web UI at localhost:8001](http://localhost:8001) and paste the generated token in the login screen.

## Section 3 — Creating Pods

## Section 4 — Creating Deployments

## Section 5 — Creating Services

### Service Types

1. ClusterIP
   
   Default

2. NodePort

   Map a static port to each Node's IP. Uses ports 30000-32767.

3. LoadBalancer

   Provision an external IP to act as a load balancer for the service.

4. ExternalName
   
   Maps a service to a DNS Name

### Creating a Service with kubectl

Ostensibly, when using `kubectl port-forward [pod/podname | deployment/deployment-name] external-port:service-port` 
a service is created.

#### Working Deployment

```shell
kaf section5/apache.deployment.yml
```

Port forward to a single pod in a cluster.

```shell
k get all
```

Select a pod-name and run the following:

```shell
k port-forward pod/pod-name 8080:80
```

Port forward to nodes in a deployment

```shell
k get all deployments
```

Select a deployment and run the following:

```shell
kpf deployment/deployment-name 8080:80 # kpf is an alias for kubectl port-forward
```

### Creating a Service with YAML

A general Service template in YAML follows:

```yaml
apiVersion: v1
kind: Service
metadata:
spec:
  type: # ClusterIP (default), NodePort, LoadBalancer

  selector:  # Pod template labels to apply to
  
  ports: # container target point and port for the service
```

A completed YAML for LoadBalancer might look like:

```yaml
apiVersion: v1
kind: Service
metadata:
   name: frontend
   labels:
     app: frontend
spec:
  type: LoadBalancer
  selector:  # Pod template labels to apply to
    app: frontend
  ports: # container target point and port for the service
    - name: http
      port: 80
      targetPort: 80
```

### kubectl and Services

For a given Kubernetes configuration file specifying a service, e.g., `k create|apply -f file.service.yml` the 
default service will be `ClusterIP`.

#### Testing a Service and Pod with `curl`

Shell into a Pod and test the URL.

```shell
k exec pod-name -- curl -s http://podIP
```

Where a pod's OS may not provide `curl`, e.g., Alpine, install it as follows:

```shell
k exec pod-name -it sh
apk add curl
curl -s http://podIP
```

### kubectl Services in Action

#### curl Tests

With a [given deployment](section5/apache.deployment.yml), add a standalone pod to the cluster as follows:

```shell
k run apache-standalone --image=httpd:alpine
```

Since this use Alpine Linux, install `curl` as shown above in [Testing a Service and Pod with `curl`](#testing-a-service-and-pod-with-curl).

Connect to the standalone pod:

```shell
k exec pod/apache-standalone -it sh
```

Open a new terminal to query a pod in the previous deployment and retrieve its `podIP` as follows:

```shell
k get pod/frontend-56d696bd65-2dhqj -o json | jq '.status.podIP'
```

In the standalone apache pod, run `curl` as follows:

```shell
 curl -s http://10.1.0.46
```

This should return the response of the Apache server running in the deployment.

#### Service Example - ClusterIP

Run the [clusterIP](section5/clusterIP.service.yml) as follows:

```shell
k apply -f section5/clusterIP.service.yml
```

Note this ClusterIP service will only apply to the deployment af apache pods, by the label `frontend`.

View the created service as follows:

```shell
k get service apache-clusterIP
```

Note the output:

```shell
NAME               TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
apache-clusterip   ClusterIP   10.106.85.204   <none>        8080/TCP   14s
```

This will LOAD BALANCE across the cluster ONLY by IP, here `10.109.85.204`, or name, here `apache-clusterip`, on 
port `8080`.

Connect to the standalone pod:

```shell
k exec pod/apache-standalone -it sh
```

Run the following commands to see the ClusterIP service in action:

```shell
curl http://10.106.85.204:8080
curl http://apache-clusterip:8080
```

#### Service Example - NodePort

Clean up the ClusterIP service created as follows:

```shell
k delete service/apache-clusterip
```

Run the provided `nodeport.service.yml` as follows:

```shell
kaf section5/nodeport.service.yml
```

Describe the service as follows and note the ports exposed as follows:

```shell
k get service/apache-nodeport
```

Open a browser to http://localhost:31000 to see this service in action.

#### Service Example - LoadBalancer

Clean up the NodePort service created as follows:

```shell
k delete service/apache-nodeport
```

Run the provided [loadbalancer.service.yml](section5/loadbalancer.service.yml) as follows:

```shell
kaf section5/loadbalancer.service.yml
```

Describe the service as follows and note the ports exposed as follows:

```shell
k get service/apache-loadbalancer
```
Open a browser to http://localhost to see this service in action.

## Section 6 - Understanding Storage Options

### Core Concepts

A **volume** can be used hold data and state for Pods and containers. 

Pods can have multiple volumes attached and use `mountPath` to access a volume.

Kubernetes supports:

- Volumes
- PersistentVolumes
- PersistentVolumeClaims
- StorageClasses

   A StorageClass is akin to a template for use by Pods and Containers

### Volumes

Similar to Docker volumes.

#### Volume Types

- emptyDir

   Empty directory for storing "transient" data (shares a Pod's lifetime) useful for sharing files between containers 
  running in a pod

- hostPath

   Pod mounts into the node's filesystem

- nfs

   An NFS (Network File System) share mounted into the Pod

- configMap/secret

   Special types of volumes that provide a Pod with access to Kubernetes resources

- persistentVolumeClaim

   Provides Pods with a more persistent storage option that is abstracted from the details.

- Cloud — Cluster-wide storage

#### Volume Type examples

##### emptyDir

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: httpd-alpine-emptydir-volume
spec:
  volumes:
    - name: html
      emptyDir: {}
  containers:
    - name: apache-emptydir
      image: httpd:alpine
      volumeMounts:
        - mountPath: /usr/local/apache2/htdocs
          name: html
          readOnly: true
      resources: {}
    - name: html-updater
      image: alpine
      command: ["/bin/sh", "-c"]
      args:
         - while true; do date >> /html/index.html; sleep 10; done
      resources: {}
      volumeMounts:
         - name: html
           mountPath: /html
```

Test this [descriptor](section6/apache-emptydir.pod.yaml) as follows:

```shell
kaf section6/apache-emptydir.pod.yaml
```

###### Viewing a Pod's Volumes

Get or Describe the Pod normally. Examples below using `jq`.

```shell
 k get pods/httpd-alpine-emptydir-volume -o json | jq '.spec.volumes' 
```

##### hostPath

This volume mounts resources on the cluster host. 

```yaml
apiVersion: v1 
kind: Pod 
metadata:
  name: docker-volume
spec: 
  volumes: 
  - name: docker-socket 
    hostPath:
      path: /var/run/docker.sock
      type: Socket
  containers: 
  - name: docker 
    image: docker 
    command: ["sleep"] 
    args: ["100000"]
    volumeMounts: 
    - name: docker-socket 
      mountPath: /var/run/docker.sock 
    resources: {}
```

In the above example, the type is `Socket`. Other types include: `DirectoryOrCreate`, `Directory`, `FileOrCreate`, 
`File`, `CharDevice`, and `BlockDevice`.

Test this [descriptor](section6/docker-hostPath.pod.yml) as follows:

```shell
kaf section6/docker-hostPath.pod.yml
```

This example is much like Docker-in-Docker and connecting to the host `/var/run/docker.sock`. Calls to `docker` then 
will produce information about the hosts runtime.

The `command` shown in the descriptor keeps the container "alive." This allows the following:

```shell
k exec docker-volume -it sh
docker version
docker ps -a # shows host containers 
```

**Caution**

This is locked to the worker node where this pod is running. A pod that gets rescheduled could cause a problem. If 
this is a concern, consider network file based options, e.g., NFS.

#### Persistent Volumes and Persistent Volume Claims

A PersistentVolume (PV) is a _cluster-wide_ storage unit with a lifecycle independent of the Pod. Typically, 
configured by an administrator.

A PersistentVolumeClaim works directly with the PersistentValue in the Pod.



## Project Prerequisites

To get the most out of the template, ensure that Node JS is available. Any LTS version should be supported.

## Begin

Run the following commands:

```shell
npm install
npm run husky:install
```

## Features

### CODEOWNERS

CODEOWNERS is a file used by GitLab to provide review and approval gates key files and directories in a project. For example it is possible to require specific user approvals for changes to `CODEOWNERS` or `.gitlab-ci.yml`.

For more information on the uses of CODEOWNERS, see [Code Owners](https://docs.gitlab.com/ee/user/project/code_owners.html).

**Notable Files**
- `CODEOWNERS`

### node

The project template includes node to enable the use of [Husky](https://typicode.github.io/husky/#/). Husky enables the use of commitlint and semantic release plugins in GitLab pipelines.

Projects SHOULD update the `package.json` to reflect the project metadata.

**Notable Files**
- `package.jsan`
- `package-lock.json`

### commitlint

[commitlint](https://commitlint.js.org/#/) is a node tool that runs, via Husky's githooks for `commit-msg` to provide a quality gate. commitlint uses a functional plugin to extend commit messages beyond the default Conventional Commits Specification 1.0.0. 

**Notable Files**
- `commitlint.config.js`

### Issue and MR templates

The project includes issue and merge request templates. These templates help issue and feature reporters with the creation of the same and ensure consistency in resolutions. The templates make extensive use of project labels in the Architecture group.

**Notable files**
- .gitlab/issue_templates/default.md
- .gitlab/merge_request_templates/default.md

### Icon

GitLab supports the use of _portable_ project icons. It is possible to set the project icon in the UI, hawever, this preference is not copied to forks. A `logo.png`, JPG is also supported, can be committed to your project and provide a consistent branding element that is source code controled and portable.

